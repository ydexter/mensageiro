package com.igorsantos.mensageiro.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.igorsantos.mensageiro.R;
import com.igorsantos.mensageiro.config.ConfiguracaoFirebase;
import com.igorsantos.mensageiro.helper.UsuarioFirebase;
import com.igorsantos.mensageiro.model.Usuario;

import java.util.concurrent.TimeUnit;

public class CadastroActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    private TextInputEditText textInputLayoutTelefone;
    private TextInputEditText textInputLayoutNome;
    private TextInputEditText textInputLayoutCodigo;
    private TextInputEditText textInputLayoutCodigoPais;
    private Button botaoCadastrar;
    private Button botaoEnviarCodigo;

    String mVerificationId;

    Usuario usuario = new Usuario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        textInputLayoutTelefone = findViewById(R.id.textInputCadastrarTelefone);
        textInputLayoutNome = findViewById(R.id.textInputNome);
        textInputLayoutCodigo = findViewById(R.id.textInputCodigo);
        textInputLayoutCodigoPais = findViewById(R.id.textInputCodigoPais);
        botaoCadastrar = findViewById(R.id.buttonCadastar);
        botaoEnviarCodigo = findViewById(R.id.buttonEviarCodigo);

        botaoEnviarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviarCodigoVerificacao();
            }
        });

        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificarSignCode();
            }
        });
    }

    public void enviarCodigoVerificacao(){
        String numeroTelefone = textInputLayoutCodigoPais.getText().toString() + textInputLayoutTelefone.getText().toString();
        String nome = textInputLayoutNome.getText().toString();

        if(numeroTelefone.isEmpty()){
            Toast.makeText(getApplicationContext(), "Preencha o número de telefone!", Toast.LENGTH_SHORT).show();
            textInputLayoutTelefone.requestFocus();
            return;
        }
        if(numeroTelefone.length() < 14){
            Toast.makeText(getApplicationContext(), "Preencha com um número válido!", Toast.LENGTH_SHORT).show();
            textInputLayoutTelefone.requestFocus();
            return;
        }if(nome.isEmpty()){
            Toast.makeText(getApplicationContext(), "Preencha o nome!", Toast.LENGTH_SHORT).show();
            textInputLayoutNome.requestFocus();
            return;
        }else {
            Toast.makeText(getApplicationContext(), "Código enviado com sucesso!", Toast.LENGTH_SHORT).show();

            //auth.setLanguageCode("br");
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    numeroTelefone,
                    60,
                    TimeUnit.SECONDS,
                    this,
                    mCallbacks);
        }
    }

    public void verificarSignCode(){
        String code = textInputLayoutCodigo.getText().toString();
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        firebaseAuth = ConfiguracaoFirebase.getFirebaseAutenticacao();
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Usuário cadastrado com sucesso!", Toast.LENGTH_SHORT).show();

                    UsuarioFirebase.atualizarNomeUsuario(usuario.getNome());

                    abrirTelaPrincipal();

                    try{
                        String numeroTelefone = textInputLayoutCodigoPais.getText().toString() + textInputLayoutTelefone.getText().toString();

                        String identificadorUsuario = numeroTelefone;
                        usuario.setIdUsuario(identificadorUsuario);
                        usuario.setNome(textInputLayoutNome.getText().toString());
                        usuario.setNumero(numeroTelefone);
                        usuario.salvar();

                    }catch(Exception e){
                        e.printStackTrace();
                    }

                }else{
                            /*// Sign in failed, display a message and update the UI
                            Log.i("signInWithCredential", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(getApplicationContext(), "Código de verificação inválido!", Toast.LENGTH_SHORT).show();
                            }*/
                }
            }
        });
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            Log.i("onVerificationCompleted", "onVerificationCompleted:" + phoneAuthCredential);

            signInWithPhoneAuthCredential(phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Log.i("onVerificationFailed", "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
            }

            // Show a message and update the UI
            // ...
        }



        @Override
        public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken token) {
            super.onCodeSent(verificationId, token);
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Log.i("onCodeSent", "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            //mResendToken = token;

            // ...
        }
    };

    public void abrirTelaPrincipal(){
        Intent intent = new Intent(getApplicationContext(), PrincipalActivity.class);
        startActivity(intent);
    }
    public void abrirTelaCadastro(){
        Intent intent = new Intent(getApplicationContext(), CadastroActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser usuarioAtual = firebaseAuth.getInstance().getCurrentUser();
        if(usuarioAtual != null){
            abrirTelaPrincipal();
        }
    }
}
