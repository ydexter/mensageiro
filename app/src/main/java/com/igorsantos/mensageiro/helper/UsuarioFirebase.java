package com.igorsantos.mensageiro.helper;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.igorsantos.mensageiro.config.ConfiguracaoFirebase;
import com.igorsantos.mensageiro.model.Usuario;

public class UsuarioFirebase {

    public static String getIdentificadorUsuario(){
        FirebaseAuth usuario = ConfiguracaoFirebase.getFirebaseAutenticacao();
        String numeroTelefone = usuario.getCurrentUser().getPhoneNumber();
        String identificadorUsuario = numeroTelefone;

        return identificadorUsuario;
    }

    public static FirebaseUser getUsuarioAtual(){
        FirebaseAuth usuario = ConfiguracaoFirebase.getFirebaseAutenticacao();

        return usuario.getCurrentUser();
    }

    public static boolean atualizarNomeUsuario(String nome){
        try{
            FirebaseUser user = getUsuarioAtual();
            UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(nome).build();

            user.updateProfile(profileChangeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(!task.isSuccessful()){
                        Log.d("Perfil", "Erro ao atualizar o nome do perfil");
                    }
                }
            });

            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean atualizarFotoUsuario(Uri url){
        try{
            FirebaseUser user = getUsuarioAtual();
            UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder().setPhotoUri(url).build();

            user.updateProfile(profileChangeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(!task.isSuccessful()){
                        Log.d("Perfil", "Erro ao atualizar a foto do perfil");
                    }
                }
            });

            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static Usuario getDadosUsuarioLogado(){
        FirebaseUser firebaseUsuario = getUsuarioAtual();

        Usuario dadosUsuario = new Usuario();
        dadosUsuario.setNumero(firebaseUsuario.getPhoneNumber());
        dadosUsuario.setNome(firebaseUsuario.getDisplayName());

        if(firebaseUsuario.getPhotoUrl() == null){
            dadosUsuario.setFoto("");
        }else{
            dadosUsuario.setFoto(firebaseUsuario.getPhotoUrl().toString());
        }
        return dadosUsuario;
    }
}
